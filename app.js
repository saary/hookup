const express = require('express');
const path = require('path');
const logger = require('morgan');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const { exec } = require('child_process');
const NATS = require('nats');

const deployer = require('./default-deployer.js');

const allowedIPs = (process.env.HOOKUP_ALLOWED_IPS && process.env.ALLOWED_IPS.split(',')) || ['131.103.20.165', '131.103.20.166', '127.0.0.1'];
const checkSource = process.env.HOOKUP_CHECK_SOURCE;

const app = express();

const configProvider = process.env.HOOKUP_CONFIG_PROVIDER ? require(process.env.HOOKUP_CONFIG_PROVIDER) : require('./default-config-provider.js');

let config = { repos: {} };
let nats;

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.get('/', function(req, res) {
  res.send('Hookup is on');
});

function handleDeployData({ payload, route }) {
  const commits = payload.commits;
  const branches = {};
  let username;
  let hash;

  if (payload.ref) {
    // gitlab / github
    username = payload.user_email || (payload.pusher && payload.pusher.email);
    const refParts = payload.ref.split('/');
    const branchName = refParts.pop();
    branches[branchName] = true;
    hash = payload.checkout_sha;
    username = payload.user_email;
  }
  else if (payload.push && payload.push.changes && payload.push.changes.length) {
    // bitbucket
    username = payload.actor.username;

    payload.push.changes.forEach(function(change) {
      if (change['new'].type === 'branch') {
        branches[change['new'].name] = true;
        username = change['new'].target.author.raw;
      }
    });
  }
  else {
    console.warn('No commits found');
    console.log('payload', payload);
  }

  for (let key in config.repos) {
    let repo = config.repos[key];
    // TODO: remove req
    if (repo.route === route && ((repo.branch === '*') || (repo.branch in branches))) {      
      // this repo is updated, deploy it
      deployer.deploy(repo, Object.keys(branches), username, hash);
    }
  }
}

if (process.env.NATS_URI) {
  console.log(`Connecting to NATS at: ${process.env.NATS_URI}`);
  
  nats = NATS.connect({'servers': [process.env.NATS_URI]});
  nats.subscribe('hookup-deploy', function(msg) {
    try {
      const { payload, route } = JSON.parse(msg);
      console.log(`Received NATS deploy message for route ${route}`, payload && payload.push);
      handleDeployData({ payload, route });
    }
    catch(e) {
      console.log(msg);
      return console.error('Failed to get push details', e);
    }
  });  
}

app.post('/deploy/:route', checkSourceIPMiddleware(), function(req, res) {
  res.end();

  let payload;
  try {
    if (req.body.payload) {
      payload = JSON.parse(req.body.payload);
    }
    else {
      payload = req.body;
    }
  }
  catch(e) {
    console.log(req.body);
    return console.error('Failed to get push details', e);
  }

  handleDeployData({ payload, route: req.params.route });
  if (nats) {
    nats.publish('hookup-deploy', JSON.stringify({ payload, route: req.params.route }));
  }
});

app.get('/restart/:route', checkSourceIPMiddleware(), function(req, res) {
  res.end();

  for (var key in config.repos) {
    var repo = config.repos[key];
    if (repo.route === req.params.route) {      
      // make sure we have an executor connected
      if (!repo._executor) {
        repo._executor = (repo.executor && repo.executor.type)? require(repo.executor.type) : require('./default-executor.js');
      }

      return repo._executor.restart(repo);
    }
  }  
});

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.json({
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.json({
    message: err.message,
    error: {}
  });
});

function checkSourceIPMiddleware() {
  return function(req, res, next) {
    if (checkSource) {
      if (allowedIPs.includes(req.connection.remoteAddress)) {
        return next();
      }

      if (allowedIPs.includes(req.headers.X-Real-IP)) {
        return next();
      }

      console.warn('IP %s is not allowed', req.connection.remoteAddress);
      res.statusCode = 403;
      return res.end();      
    }

    return next();
  };
}

deployer.on('preDeploy', function(repo, cb) {
  // make sure we have an executor connected
  if (!repo._executor) {
    repo._executor = (repo.executor && repo.executor.type)? require(repo.executor.type) : require('./default-executor.js');
  }

  repo._executor.preDeploy(repo, cb);
});

deployer.on('deploy', function(repo, url) {
  console.log('Deploying: ', url);
});

deployer.on('deploySuccess', function(repo, username, branch, hash) {
  repo.port = repo.port || 80;
  console.log('Deploy success.\nRepo is available at: %s\nRestarting package on port %s\nUser: %s\nBranch:', repo.deployPath, repo.port, username, branch);
  if (process.env.HOOKUP_POST_DEPLOY_COMMAND) {
    exec(process.env.HOOKUP_POST_DEPLOY_COMMAND, {cwd: repo.deployPath}, (err, stdout, stderr) => {
      if (err) console.error(err);
      console.log('>>> STDOUT');
      console.log(stdout);
      console.error('>>> STDERR');
      console.error(stderr);
      repo._executor.restart(repo, username, branch, hash);
    });
  }
  else {
    repo._executor.restart(repo, username, branch, hash);
  }
});

function loadConfig(cb) {
  configProvider.get();
}

configProvider.on('update', (_config) => {
  config = _config;
  console.log('Config updated: ', config);
  console.log('Deploying repos');
  deployAll();
});

configProvider.on('addRepo', (_config) => {
  const repos = _config.repos;
  const repoName = Object.keys(repos)[0];
  const repo = repos[repoName];

  if (!repo) {
    console.warn('Repo is missing: ', _config);
    return;
  }

  if (config.repos[repoName]) {
    console.log('Updated repo: ', repo);
    config.repos[repoName] = repo;
    return;
  }

  console.log('New repo: ', repo);
  config.repos[repoName] = repo;

  console.log('Deploying the repo');
  deployer.deploy(repo);
});

configProvider.on('removeRepo', ({ containerId }) => {
  const repos = config.repos;
  const repoName = Object.keys(repos).filter(key => repos[key].containerId === containerId)[0];
  const repo = repos[repoName];

  if (repoName && !repo.isPassive) {
    delete repos[repoName];
    console.log('Removing repo: ', repo);
  }
});

function deployAll() {
  const repos = Object.keys(config.repos).map(function(key) { return config.repos[key]; });
  deployer.deployAll(repos);
}

app.deployAll = deployAll;
app.loadConfig = loadConfig;

module.exports = app;
