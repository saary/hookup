//
// Default deployer
//
// A deployer is an EventEmitter that emits: 'preDeploy', 'deploy', 'deployComplete', 'deployError' and 'deploySuccess'
// The default deployer uses girror (https://github.com/eladb/node-girror) as a clean deployment mechanism
//

const EventEmitter = require('events');
const girror = require('girror');
const path = require('path');
const reposFolder = process.env.HOOKUP_REPO_FOLDER || '/';

const emitter = new EventEmitter();

let awaiting = [];

function deployAwaiting() {
  if (awaiting.length) {
    var deployAction = awaiting[0];
    if (deployAction) {
      deployAction();
      return emitter.once('deployComplete', function() {
        awaiting.shift();
        deployAwaiting();
      });
    }

    awaiting.shift();
    return deployAwaiting();    
  }
}

emitter.deploy = function(repo, pushBranches, pushUsername, hash) {
  function deployAction() {
    // let each of the pre deploy listeners to run before the actual deployment
    var count = emitter.listeners('preDeploy').length;  
    var preDeployDone = function() {
      count--;
      if (count <= 0) {
        deploy(repo, pushBranches, pushUsername, hash);
      }
    };

    if (count) {
      emitter.emit('preDeploy', repo, preDeployDone);
    }
    else {
      // no listeners deploy immediately
      deploy(repo);
    }    
  }

  awaiting.push(deployAction);
  // try to drain awaiting just if this is the only element
  if (awaiting.length === 1) {
    return deployAwaiting();
  }
};

emitter.deployAll = function(repos) {
  var index = 0;
  function deployNext() {
    if (index < repos.length) {
      var repo = repos[index];
      index++;
      emitter.deploy(repo);
      emitter.once('deployComplete', deployNext);
    }
  }

  deployNext();
};

function deploy(repo, pushBranches, pushUsername, hash) {
  var firstPushBranch = (pushBranches && pushBranches.length && pushBranches[0]) || 'master';
  var deployedBranch = repo.branch === '*'? firstPushBranch: repo.branch;
  var url = repo.url + '#' + deployedBranch;
  
  emitter.emit('deploy', repo, url, pushUsername, deployedBranch);

  repo.deployPath = repo.deployPath || path.join(reposFolder, repo.workdir);
  console.log(`Deploying ${repo} to ${repo.deployPath}`);
    
  return girror(url, repo.deployPath, function(err) {
    emitter.emit('deployComplete', err);

    if (err) {
      console.error(`Deploying ${repo} failed`, err);
      return emitter.emit('deployError', err, repo, pushUsername, deployedBranch, hash);
    }

    console.info(`Deployed ${repo} successfully`);
    return emitter.emit('deploySuccess', repo, pushUsername, deployedBranch, hash);
  });    
}

module.exports = emitter;
