//
// Default executor
//
// An executor is responsible for running the deployed entity. 
// It is exports 3 main functions:
//   preDeploy - Any pre-deploy actions that should take place
//   restart -  Restarts the given repo deployment 
//
// The default executor is very basic and it executes 'npm start' and 'npm stop' in the deployed folder

var exec = require('child_process').exec;

function getEnv() {
  var env = {};
  for(var k in process.env) {
    env[k] = process.env[k];
  }

  return env;
}

exports.preDeploy = function(repo, cb) {
  if (repo.executor === false) {
    return cb();
  }

  // run pre-deploy script
  if (repo.predeployCommand) {
    var env = getEnv();
    return exec(repo.predeployCommand, {cwd: repo.deployPath, env: env}, function(err) {
      if (err) {
        console.error(err);
      }

      return cb();
    });
  }

  return cb();
};

exports.restart = function(repo) {
  if (repo.executor === false) {
    return;
  }

  var env = getEnv();
  // best effort to restart
  env.PORT = repo.port;

  var stopCommand = repo.stopCommand || 'npm stop';
  var startCommand = repo.startCommand || 'npm start';

  exec(stopCommand, {cwd: repo.deployPath, env: env}, function(err) {
    if (err) {
      console.error(err);
    }

    exec(startCommand, {cwd: repo.deployPath, env: env}, function(err) {
      if (err) {
        console.error(err);
      }
    });
  });
};