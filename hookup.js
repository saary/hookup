#!/usr/bin/env node
var debug = require('debug')('hookup');
var app = require('./app');

app.set('port', process.env.PORT || 3000);

var server = app.listen(app.get('port'), function() {
  debug('Hookup listening on port ' + server.address().port);
});

// start by deploying all repos
app.loadConfig();
