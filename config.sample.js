exports.repos = {
  repo_nickname_prod: {
    url: 'git@bitbucket.org:saary/hookup',
    route: 'my-top-secret-deploy-name',
    branch: 'prod',
    workdir: '/web_prod',
    port: 80,
  },
  repo_nickname_dev: {
    url: 'git@bitbucket.org:saary/hookup',
    route: '075384b9-c59a-4ff3-b9e3-7901531fb670',
    branch: 'master',
    workdir: '/web_dev',
    port: 3001,
  },
};