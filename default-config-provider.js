//
// Default config provider
//
// A config provider is a service responsible for providing the config for hookup
// The default provider looks for a local config file
//

const EventEmitter = require('events');

const emitter = new EventEmitter();

const get = ( cb = ()=>{} ) => {
  const configPath = process.env.HOOKUP_CONFIG || './config.js';  
  
  try {
    const config = require(configPath);
    cb(null, config);
    
    emitter.emit('update', config);
    return Promise.resolve(config);
  }
  catch(e) {
    cb(e);
    return Promise.reject(e);
  }
};

process.on('SIGHUP', () => {
  console.log('Reloading config ...');
  get();
});

emitter.get = get;

module.exports = emitter;
